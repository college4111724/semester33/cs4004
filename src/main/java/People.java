public class People {
    private String name;
    private String email;
    private String phone;

    public People(String name, String email, String phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isPhoneNumberValid() {
        phone.replaceAll(" ", "");
        if (getPhone().startsWith("+353") && getPhone().length() == 14) {
            return true;
        }
        return false;
    }
}
